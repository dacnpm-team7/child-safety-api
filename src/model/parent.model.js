const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Child = require('./child.model');

var ParentSchema = new mongoose.Schema(
    {
        username: String,
        password: String,
        children: [],
        key: String,
        display_name: String,
        email: String,
        date_of_birth: String,
        gender: String,
        facebook_id: String,
        google_id: String,
        avatar: {
            type: String,
            default: '/uploads/default_avatar.png'
        },
        role: { type: String, default: 'parent' }
    },
    {
        timestamps: true
    }
);

var parents = mongoose.model('parents', ParentSchema);

module.exports = parents;