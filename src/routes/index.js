const express = require('express');
const router = express.Router();

const parent = require('./parent.route');
const auth = require('./auth.route');
const upload = require('./upload.route');

module.exports = () => {
    parent(router);
    auth(router);
    upload(router);
    
    return router;
}