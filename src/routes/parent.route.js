const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const passport = require('../passport/passport');
const Parent = require('../model/parent.model');
const Child = require('../model/child.model');
const Location = require('../model/location.model');
const config = require('../config');
const verify = require('../middlewares/auth.middleware');
const apiAdapter = require('./apiAdapter');

module.exports = (app) => {
    app.use('/parents', router);

    router.get('/me', verify, async (req, res) => {
        const { username } = req.tokenPayload;

        const charCodeOfFirstElement = username.toLowerCase().charCodeAt(0)

        if (charCodeOfFirstElement < 97 && charCodeOfFirstElement > 122) {
            return res.status(401).json({ message: 'invalid format of username' });
        }

        if (charCodeOfFirstElement < 110) {
            try {
                const resp = await apiAdapter(config.server1_url).get(`/api/parents${req.path}`, {
                    headers: {
                        "x-access-token": req.headers['x-access-token']
                    }
                });
                res.status(resp.status).json(resp.data);
            } catch (error) {
                res.status(error.response.status).json(error.response.data);
            }
        }
        else {
            try {
                const resp = await apiAdapter(config.server2_url).get(`/api/parents${req.path}`, {
                    headers: {
                        "x-access-token": req.headers['x-access-token']
                    }
                });
                res.status(resp.status).json(resp.data);
            } catch (error) {
                res.status(error.response.status).json(error.response.data);
            }
        }
    });

    router.get('/block', verify, async (req, res) => {
        const { username } = req.tokenPayload;

        const charCodeOfFirstElement = username.toLowerCase().charCodeAt(0)

        if (charCodeOfFirstElement < 97 && charCodeOfFirstElement > 122) {
            return res.status(401).json({ message: 'invalid format of username' });
        }

        if (charCodeOfFirstElement < 110) {
            try {
                const resp = await apiAdapter(config.server1_url).get(`/api/parents${req.path}`, {
                    headers: {
                        "x-access-token": req.headers['x-access-token']
                    }
                });
                res.status(resp.status).json(resp.data);
            } catch (error) {
                res.status(error.response.status).json(error.response.data);
            }
        }
        else {
            try {
                const resp = await apiAdapter(config.server2_url).get(`/api/parents${req.path}`, {
                    headers: {
                        "x-access-token": req.headers['x-access-token']
                    }
                });
                res.status(resp.status).json(resp.data);
            } catch (error) {
                res.status(error.response.status).json(error.response.data);
            }
        }
    });

    router.patch('/info', verify, async (req, res) => {
        const { username } = req.tokenPayload;

        const charCodeOfFirstElement = username.toLowerCase().charCodeAt(0)

        if (charCodeOfFirstElement < 97 && charCodeOfFirstElement > 122) {
            return res.status(401).json({ message: 'invalid format of username' });
        }

        if (charCodeOfFirstElement < 110) {
            try {
                const resp = await apiAdapter(config.server1_url).patch(`/api/parents${req.path}`, req.body, {
                    headers: {
                        "x-access-token": req.headers['x-access-token']
                    }
                });
                res.status(resp.status).json(resp.data);
            } catch (error) {
                res.status(error.response.status).json(error.response.data);
            }
        }
        else {
            try {
                const resp = await apiAdapter(config.server2_url).patch(`/api/parents${req.path}`, {
                    headers: {
                        "x-access-token": req.headers['x-access-token']
                    }
                });
                res.status(resp.status).json(resp.data);
            } catch (error) {
                res.status(error.response.status).json(error.response.data);
            }
        }
    });

    router.post('/child', verify, async (req, res) => {
        const { username } = req.tokenPayload;

        const charCodeOfFirstElement = username.toLowerCase().charCodeAt(0)

        if (charCodeOfFirstElement < 97 && charCodeOfFirstElement > 122) {
            return res.status(401).json({ message: 'invalid format of username' });
        }

        if (charCodeOfFirstElement < 110) {
            try {
                const resp = await apiAdapter(config.server1_url).post(`/api/parents${req.path}`, req.body, {
                    headers: {
                        "x-access-token": req.headers['x-access-token']
                    }
                });
                res.status(resp.status).json(resp.data);
            } catch (error) {
                res.status(error.response.status).json(error.response.data);
            }
        }
        else {
            try {
                const resp = await apiAdapter(config.server2_url).post(`/api/parents${req.path}`, {
                    headers: {
                        "x-access-token": req.headers['x-access-token']
                    }
                });
                res.status(resp.status).json(resp.data);
            } catch (error) {
                res.status(error.response.status).json(error.response.data);
            }
        }
    });

    router.get('/child/:child_key/location', verify, async (req, res) => {
        const { username } = req.tokenPayload;

        const charCodeOfFirstElement = username.toLowerCase().charCodeAt(0)

        if (charCodeOfFirstElement < 97 && charCodeOfFirstElement > 122) {
            return res.status(401).json({ message: 'invalid format of username' });
        }

        if (charCodeOfFirstElement < 110) {
            try {
                const resp = await apiAdapter(config.server1_url).get(`/api/parents${req.path}`, {
                    headers: {
                        "x-access-token": req.headers['x-access-token']
                    }
                });
                res.status(resp.status).json(resp.data);
            } catch (error) {
                res.status(error.response.status).json(error.response.data);
            }
        }
        else {
            try {
                const resp = await apiAdapter(config.server2_url).get(`/api/parents${req.path}`, {
                    headers: {
                        "x-access-token": req.headers['x-access-token']
                    }
                });
                res.status(resp.status).json(resp.data);
            } catch (error) {
                res.status(error.response.status).json(error.response.data);
            }
        }
    });

    router.post('/child/:child_key/location', verify, async (req, res) => {
        const { username } = req.tokenPayload;

        const charCodeOfFirstElement = username.toLowerCase().charCodeAt(0)

        if (charCodeOfFirstElement < 97 && charCodeOfFirstElement > 122) {
            return res.status(401).json({ message: 'invalid format of username' });
        }

        if (charCodeOfFirstElement < 110) {
            try {
                const resp = await apiAdapter(config.server1_url).post(`/api/parents${req.path}`, req.body, {
                    headers: {
                        "x-access-token": req.headers['x-access-token']
                    }
                });
                res.status(resp.status).json(resp.data);
            } catch (error) {
                res.status(error.response.status).json(error.response.data);
            }
        }
        else {
            try {
                const resp = await apiAdapter(config.server2_url).post(`/api/parents${req.path}`, {
                    headers: {
                        "x-access-token": req.headers['x-access-token']
                    }
                });
                res.status(resp.status).json(resp.data);
            } catch (error) {
                res.status(error.response.status).json(error.response.data);
            }
        }
    });
};