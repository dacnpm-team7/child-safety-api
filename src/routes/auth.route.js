const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const passport = require('../passport/passport');
const Parent = require('../model/parent.model');
const config = require('../config');
const apiAdapter = require('./apiAdapter');

const api = apiAdapter(config.server1_url);

module.exports = (app) => {
    app.use('/auth', router);

    router.post('/signup', async (req, res) => {
        const { username } = req.body;

        const charCodeOfFirstElement = username.toLowerCase().charCodeAt(0)

        if (charCodeOfFirstElement < 97 && charCodeOfFirstElement > 122) {
            return res.status(401).json({ message: 'invalid format of username' });
        }

        if (charCodeOfFirstElement < 110) {
            try {
                const resp = await apiAdapter(config.server1_url).post(`/api/auth${req.path}`, req.body);
                res.status(resp.status).json(resp.data);
            } catch (error) {
                res.status(error.response.status).json(error.response.data);
            }
        }
        else {
            try {
                const resp = await apiAdapter(config.server2_url).post(`/api/auth${req.path}`, req.body);
                res.status(resp.status).json(resp.data);
            } catch (error) {
                res.status(error.response.status).json(error.response.data);
            }
        }
    });

    router.post('/signin', async (req, res) => {
        const { username } = req.body;

        const charCodeOfFirstElement = username.toLowerCase().charCodeAt(0)

        if (charCodeOfFirstElement < 97 && charCodeOfFirstElement > 122) {
            return res.status(401).json({ message: 'invalid format of username' });
        }

        if (charCodeOfFirstElement < 110) {
            try {
                const resp = await apiAdapter(config.server1_url).post(`/api/auth${req.path}`, req.body);
                res.status(resp.status).json(resp.data);
            } catch (error) {
                res.status(error.response.status).json(error.response.data);
            }
        }
        else {
            try {
                const resp = await apiAdapter(config.server2_url).post(`/api/auth${req.path}`, req.body);
                res.status(resp.status).json(resp.data);
            } catch (error) {
                res.status(error.response.status).json(error.response.data);
            }
        }
    });
};